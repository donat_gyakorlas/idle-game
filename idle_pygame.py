"""
update 0.8.1
"""

import pygame
from pygame import mixer
import random
import math

mixer.init()

# ToDo: organize, organize, organize

# global variables
version = '0.8.1'
screen_width = 360
screen_heigth = 640
button_pressed = False  # true if a button is currently pressed (this is the easiest way, I know it's ugly)
money = 0               # starting money
level = 1               # starting level
factories = 0           # factories auto-generate money
product_value = 1       # value of one product
sold_products = 0       # number of sold products
laziness = 1000         # lazines off factory workers (basically the denominator to productivity)
isr = 0                 # investment success rate
thrift = 0              # percentage of upgrade expenses given back
infobox = False         # on/off switch for infoboxes
antialiasing = True
sound_on = False
dark_mode = False
prestige_prices = (1000, 30000, 140000, 600000, 1500000, 999999999999)

boxes = (
    pygame.image.load("images\\box_images\\box1.png"),
    pygame.image.load("images\\box_images\\box2.png"),
    pygame.image.load("images\\box_images\\box3.png"),
    pygame.image.load("images\\box_images\\box4.png"),
    pygame.image.load("images\\box_images\\box5.png"),
    pygame.image.load("images\\box_images\\box6.png")
)


littleboxes = (
    pygame.image.load("images\\littlebox_images\\littlebox_1.png"),
    pygame.image.load("images\\littlebox_images\\littlebox_2.png"),
    pygame.image.load("images\\littlebox_images\\littlebox_3.png"),
    pygame.image.load("images\\littlebox_images\\littlebox_4.png"),
    pygame.image.load("images\\littlebox_images\\littlebox_5.png"),
    pygame.image.load("images\\littlebox_images\\littlebox_6.png"),
)


active_click_sound_index = 0
click_sounds = (
    mixer.Sound('sounds\\click_1.wav'),
    mixer.Sound('sounds\\click_2.wav'),
    mixer.Sound('sounds\\click_3.wav'),
    mixer.Sound('sounds\\click_4.wav'),
    mixer.Sound('sounds\\click_5.wav')
)


class Text:
    def __init__(self, value, size, x=None, y=None):
        # x == None --> centered vertically
        # y == None --> centered horizontally
        self.value = value
        self.x = x
        self.y = y
        self.size = size

    def set_value(self, value):
        self.value = value
        self.x = (screen_width / 2) - (pygame.font.Font('freesansbold.ttf', self.size).render(self.value, True, ((255, 255, 255) if dark_mode else (0, 0, 0))).get_width() / 2)

    def draw_text(self):
        font = pygame.font.Font('freesansbold.ttf', self.size)
        rendered = font.render(self.value, antialiasing, ((255, 255, 255) if dark_mode else (0, 0, 0)))
        if self.x is None:
            self.x = (screen_width / 2) - (rendered.get_width() / 2)
        if self.y is None:
            self.y = (screen_heigth / 2) - (rendered.get_height() / 2)
        screen.blit(rendered, (int(self.x), int(self.y)))


class Button:
    def __init__(self, text, x, y, width=200, heigth=75, text_size=30, associated_upgrade=None):
        self.x = x
        self.y = y
        self.text = text
        self.width = width
        self.heigth = heigth
        self.text_size = text_size
        self.active = False     # True if button is on screen, needed for overlapping frames with buttons
        self.associated_upgrade = associated_upgrade  # if it's an upgrade button, that upgrade is associated with the button object through this variable

    @property
    def get_width(self):
        return self.width

    @property
    def get_heigth(self):
        return self.heigth

    @property
    def get_upgrade(self):
        return self.associated_upgrade

    def set_text(self, text):
        self.text = text

    def activate(self):
        self.active = True

    def deactivate(self):
        self.active = False

    def cursor_collide(self):
        button_rect = pygame.Rect(self.x, self.y, self.width, self.heigth)
        return button_rect.collidepoint(pygame.mouse.get_pos()) and self.active

    def render_text(self, price=None):  # if it's an upgrade button, "price" sets a price to be displayed
        font = pygame.font.Font('freesansbold.ttf', self.text_size)
        text_img = font.render(self.text, antialiasing, (255, 255, 255))
        text_len = text_img.get_width()
        if price is not None:
            price_text_img = pygame.font.Font('freesansbold.ttf', self.text_size+5).render(price, antialiasing, (255, 255, 255))
            price_text_len = price_text_img.get_width()
            screen.blit(price_text_img, (int(self.x + self.width / 2 - price_text_len / 2), int(self.y + (3*self.heigth/5-5))))
        screen.blit(text_img, (int(self.x + self.width / 2 - text_len / 2), int(self.y + (self.heigth / (3 if price is None else 5)))))

    def draw_button(self):
        global button_pressed
        action = False
        cursor_position = pygame.mouse.get_pos()
        button_rect = pygame.Rect(self.x, self.y, self.width, self.heigth)
        colors = ((180, 77, 77), (146, 60, 60), (100, 60, 60))

        # cursor detection
        if button_rect.collidepoint(cursor_position) and self.active:
            if pygame.mouse.get_pressed(3)[0]:
                button_pressed = True
                pygame.draw.rect(screen, colors[2], button_rect)
            elif not pygame.mouse.get_pressed(3)[0] and button_pressed:
                button_pressed = False
                action = True
            else:
                pygame.draw.rect(screen, colors[1], button_rect)
        else:
            pygame.draw.rect(screen, colors[0], button_rect)

        if sound_on and action:
            click_sounds[active_click_sound_index].play()
        return action


class Frame:
    def __init__(self, name, *things, background_color=(255, 255, 255)):
        self.name = name
        self.color = background_color
        self.things = things

    def draw_frame(self):
        screen.fill((40, 40, 40) if dark_mode else (255, 255, 255))
        if self.name == 'Home Screen':
            screen.blit(boxes[level - 1], (screen_width // 2 - 64, 325))
        elif self.name == 'Upgrades':
            # prestige progress bar
            pygame.draw.rect(
                screen,
                (255, 0, 0) if money < prestige_prices[level-1] else (0, 220, 0),
                pygame.Rect(30, 88, int((300/prestige_prices[level-1])*money) if money <= prestige_prices[level-1] else 300, 5)
            )
            screen.blit(littleboxes[level - 1], (45, 588))
        for thing in self.things:
            if isinstance(thing, Button):
                thing.activate()
                thing.draw_button()
            elif isinstance(thing, Text):
                thing.draw_text()

    def hide_frame(self):
        for thing in self.things:
            if isinstance(thing, Button):
                thing.deactivate()


# just to organise the upgrade prices, levels etc.
class Upgrade:
    def __init__(self, name, prices, description, upgrade_level=1):
        self.name = name
        self.prices = prices
        self.description = description
        self.level = upgrade_level

    @property
    def get_name(self):
        return self.name

    @property
    def get_prices(self):
        return self.prices

    @property
    def get_description(self):
        return self.description

    @property
    def get_level(self):
        return self.level

    def current_price(self):
        if self.level <= len(self.prices):
            return f'${self.prices[self.level - 1]}'
        else:
            return '(max level)'

    def current_price_numeric(self):
        if self.level <= len(self.prices):
            return self.prices[self.level - 1]


if __name__ == '__main__':
    pygame.init()

    screen = pygame.display.set_mode((screen_width, screen_heigth))
    pygame.display.set_caption("Box Idle")
    pygame.display.set_icon(pygame.image.load("images\\box_idle_icon.png"))

    # text objects
    t11 = Text('Idle Játék', 60, x=None, y=50)
    t12 = Text('készítette: Szabados Donát', 20, x=None, y=150)
    t21 = Text(f'${money}', 80, x=None, y=80)
    t22 = Text(f'level {level}', 25, x=None, y=30)
    t23 = Text(f'termék értéke: {product_value}', 25, x=None, y=185)
    t24 = Text(f'gyárak száma: {factories}', 25, x=None, y=225)
    t25 = Text(f'eladott termékek: {sold_products}', 25, x=None, y=265)
    t31 = Text(f'${money}', 35, x=None, y=25)
    t41 = Text(f'v{version}', 25, x=30, y=30)

    # upgrades
    u1 = Upgrade('gyár építése', (50, 500, 5000, 50000, 500000, 5000000), "Minden gyár egy mp alatt egy\nterméket gyárt magától\n(Passzív bevételi forrás)")
    u2 = Upgrade('termék fejlesztése', (10, 20, 30, 200, 1000, 5000, 10000, 20000, 30000, 50000, 100000, 200000, 500000), "Növeli az általad gyártott\ntermék értékét\n(+100%)")
    u3 = Upgrade('béremelés', (10000, 100000, 250000, 500000, 1000000), "Motiválod a gyármunkásokat,\nígy azok gyorsabban dolgoznak\nMinél több gyárad van, annál\njobban megéri\n(+10%)")
    u4 = Upgrade('befektetés', (500, 2500, 10000, 30000, 70000, 150000, 250000, 500000, 800000, 1000000), "Esély arra, hogy kézi gyártás\nsorán sokszoros áron adsz el\negy terméket\n(+4%)")
    u5 = Upgrade('szerencsejáték', (100, 200, 500, 1000, 2000, 5000, 7500, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000), "Szerencsejáték szelvény\nvásárlásával 25% esélyed van\nnagyon sok pénzt nyerni")
    u6 = Upgrade('takarékosság', (500, 3000, 10000, 50000), "Minden fejlesztésért kiadott\npénzedből valamennyit\nvisszakapsz\n(+5%)")
    u7 = Upgrade('upgrade 7', tuple(), "Upgrade kitalálás\nin progress")
    u8 = Upgrade('upgrade 8', tuple(), "Upgrade kitalálás\nin progress")
    u_prestige = Upgrade('szintlépés', prestige_prices, "Amikor szintet lépsz, teljesen\nelölről kezdesz mindent,\nviszont a terméked minden\nszint után jóval többet fog\nérni", upgrade_level=level)

    # buttons
    b11 = Button('Játék', x=80, y=250)
    b12 = Button('Beállítások', x=80, y=360)
    b13 = Button('Kilépés', x=80, y=470)
    b21 = Button('Fejlesztések', x=20, y=20, width=100, heigth=40, text_size=15)
    b22 = Button('Főmenü', x=240, y=20, width=100, heigth=40, text_size=15)
    b23 = Button('Kézi gyártás', x=55, y=485, width=250, heigth=100, text_size=35)
    b31 = Button('Vissza', x=270, y=20, width=70, heigth=40, text_size=14)
    b32 = Button('gyár építése', x=20, y=120, width=150, heigth=75, text_size=15, associated_upgrade=u1)
    b33 = Button('termék fejlesztése', x=20, y=215, width=150, heigth=75, text_size=15, associated_upgrade=u2)
    b34 = Button('béremelés', x=20, y=310, width=150, heigth=75, text_size=15, associated_upgrade=u3)
    b35 = Button('befektetés', x=20, y=405, width=150, heigth=75, text_size=15, associated_upgrade=u4)
    b36 = Button('szerencsejáték', x=190, y=120, width=150, heigth=75, text_size=15, associated_upgrade=u5)
    b37 = Button('takarékosság', x=190, y=215, width=150, heigth=75, text_size=15, associated_upgrade=u6)
    b38 = Button('Upgrade 7', x=190, y=310, width=150, heigth=75, text_size=15, associated_upgrade=u7)
    b39 = Button('Upgrade 8', x=190, y=405, width=150, heigth=75, text_size=15, associated_upgrade=u8)
    b310 = Button('szintlépés', x=20, y=500, width=320, heigth=75, text_size=15, associated_upgrade=u_prestige)
    b311 = Button('Info', x=20, y=20, width=70, heigth=40, text_size=14)
    b41 = Button('Vissza', x=240, y=20, width=100, heigth=40, text_size=15)
    b42 = Button(f'Szövegsimítás: {"be" if antialiasing else "ki"}', x=80, y=140, text_size=20)
    b43 = Button(f'Hangok: {"be" if sound_on else "ki"}', x=80, y=250, text_size=25)
    b44 = Button(f'Hangtípus: {active_click_sound_index + 1}', x=80, y=360, text_size=25)
    b45 = Button('Sötét mód', x=80, y=470, text_size=25)

    # frames
    f1 = Frame('Main Menu', b11, b12, b13, t11, t12)
    f2 = Frame('Home Screen', b21, b22, b23, t21, t22, t23, t24, t25)
    f3 = Frame('Upgrades', b31, b32, b33, b34, b35, b36, b37, b38, b39, b310, b311, t31)
    f4 = Frame('Beállítások', b41, b42, b43, b44, b45, t41)

    # lists of things
    button_list = [b11, b12, b13, b21, b22, b23, b31, b32, b33, b34, b35, b36, b37, b38, b39, b310, b311, b41, b42, b43, b44, b45]
    texts = [t11, t12, t21, t22, t23, t24, t25, t31]

    # the frame set here shows up on game start, for now it's the main menu
    current_frame = f1

    # events
    EVENT_FACTORY = pygame.USEREVENT + 1

    # game loop
    running = True
    clock = pygame.time.Clock()

    # limiting framerate to 60fps
    while running:
        clock.tick(60)

        # refreshes display of all statistics and money
        def refresh_numbers():
            if money < 1000000:
                money_display = f'${money}'
            elif 1000000 < money < 1000000000:
                money_display = f'${math.floor((money / 1000000) * 1000) / 1000}M'
            else:
                money_display = f'${math.floor((money / 1000000000) * 1000) / 1000}B'
            t22.set_value(f'level {level}')
            t23.set_value(f'termék értéke: {product_value}')
            t24.set_value(f'gyárak száma: {factories}')
            t25.set_value(f'eladott termékek: {sold_products}')
            t21.set_value(money_display)
            t31.set_value(money_display)

        # adds an amount to the global "money" variable
        def make_money(amount):
            global money
            money += amount * level ** 3    # effect of prestige
            refresh_numbers()

        # shows an infobox when hovering over upgrade buttons (a gray rectangle for now)
        def show_infobox(text):
            x = 15
            y = 420 if pygame.mouse.get_pos()[1] < 400 else 110
            pygame.draw.rect(screen, (220, 220, 220), pygame.Rect(x, y, 330, 200))
            lines = text.splitlines()
            for count, l in enumerate(lines):
                # I don't really understand why this works, but hey, it works!
                screen.blit(pygame.font.Font('freesansbold.ttf', 20).render(l, antialiasing, (0, 0, 0)), (x+10, y+10 + 25*count))


        # event handling
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == EVENT_FACTORY:
                sold_products += 1
                make_money(product_value)
                refresh_numbers()

        # navigation buttons
        if b11.draw_button():
            current_frame.hide_frame()
            current_frame = f2
        elif b12.draw_button():
            current_frame.hide_frame()
            current_frame = f4
        elif b13.draw_button():
            running = False
        elif b21.draw_button():
            current_frame.hide_frame()
            current_frame = f3
        elif b22.draw_button():
            current_frame.hide_frame()
            current_frame = f1
        elif b31.draw_button():
            current_frame.hide_frame()
            current_frame = f2
        elif b41.draw_button():
            current_frame.hide_frame()
            current_frame = f1

        # hand-manufacturing button
        if b23.draw_button():
            sold_products += 1
            # investment upgrade
            if isr > 0 and random.randint(isr, 25) == 25:
                make_money(level+6 ** 3)
            make_money(product_value)
            refresh_numbers()

        # price and level handling for upgrade buttons
        def upgrade(u, fun):
            global factories, money
            if u.level <= len(u.prices):
                if u.current_price_numeric() <= money:
                    money -= int(u.current_price_numeric() * (1-thrift))
                    u.level += 1
                    fun()
                else:
                    print(f'Not enough money for "{u.name}" upgrade\nprice: {u.current_price()}')
            else:
                print(f'Max level for "{u.name}" upgrade reached')

        # upgrade functions (what the upgrades actually do)
        def ufun1():    # gyár építése
            global factories
            factories += 1
            pygame.time.set_timer(EVENT_FACTORY, laziness // factories)
            refresh_numbers()

        def ufun2():    # termék fejlesztése
            global product_value
            product_value += 1
            if factories > 0:
                pygame.time.set_timer(EVENT_FACTORY, laziness // factories)
            refresh_numbers()

        def ufun3():    # béremelés
            global laziness
            laziness -= 100
            if factories > 0:
                pygame.time.set_timer(EVENT_FACTORY, laziness // factories)

        def ufun4():    # befektetés
            global isr
            if isr <= 25:
                isr += 1

        def ufun5():    # szerencsejáték
            if random.randint(1, 4) == 4:
                make_money(level+10**3)

        def ufun6():    # takarékosság
            global thrift
            thrift += 0.05

        def ufun7():
            pass

        def ufun8():
            pass

        def ufun_prestige():    # levelup/prestige
            global level, current_frame, money, factories, laziness, product_value, sold_products, isr, thrift
            if level < 6:
                level += 1
                money = 0
                factories = 0
                product_value = 1
                sold_products = 0
                laziness = 1000
                thrift = 0
                isr = 0
                for u in (u1, u2, u3, u4, u5, u6, u7, u8):
                    u.level = 1
                pygame.time.set_timer(EVENT_FACTORY, 0)
                current_frame.hide_frame()
                current_frame = f2
                refresh_numbers()

        # upgrade buttons
        button_list = [b32, b33, b34, b35, b36, b37, b38, b39, b310]
        upgrade_list = [u1, u2, u3, u4, u5, u6, u7, u8, u_prestige]
        ufun_list = [ufun1, ufun2, ufun3, ufun4, ufun5, ufun6, ufun7, ufun8, ufun_prestige]
        for i in range(len(button_list)):
            if button_list[i].draw_button():
                upgrade(upgrade_list[i], ufun_list[i])

        # option buttons
        if b42.draw_button():
            antialiasing = not antialiasing
            b42.set_text(f'Szövegsimítás: {"be" if antialiasing else "ki"}')
        elif b43.draw_button():
            sound_on = not sound_on
            b43.set_text(f'Hangok: {"be" if sound_on else "ki"}')
        elif b44.draw_button():
            active_click_sound_index += 1 if active_click_sound_index != 4 else -4
            b44.set_text(f'Hangtípus: {active_click_sound_index + 1}')
        elif b45.draw_button():
            dark_mode = not dark_mode

        # infoboxes on/off switch
        if b311.draw_button():
            infobox = not infobox

        # show the current frame
        current_frame.draw_frame()

        # render text on buttons
        for button in [b11, b12, b13, b21, b22, b23, b31, b32, b33, b34, b35, b36, b37, b38, b39, b310, b311, b41, b42, b43, b44, b45]:
            if button.active:
                if button.associated_upgrade is None:
                    button.render_text()
                else:
                    button.render_text(button.associated_upgrade.current_price())

        # show infoboxes about upgrades
        if infobox:
            for button in (b32, b33, b34, b35, b36, b37, b38, b39, b310):
                if button.cursor_collide():
                    show_infobox(button.associated_upgrade.description)

        # for the sake of safety
        refresh_numbers()

        # refresh screen
        pygame.display.update()
